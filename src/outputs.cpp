#include <Arduino.h>
#include "outputs.h"
#include "pins.h"

void setupOutputs() {
  pinMode(PIN_COOLER, OUTPUT);
  pinMode(PIN_HUMIDIFIER, OUTPUT);
  pinMode(PIN_CIRCULATION, OUTPUT);
}

void setCooling(bool state) {
  digitalWrite(PIN_COOLER, state);
}

void setCirculation(bool state) {
  digitalWrite(PIN_CIRCULATION, state);
}

void setHumidifier(bool state) {
  digitalWrite(PIN_HUMIDIFIER, state);
}
