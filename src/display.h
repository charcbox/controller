#include <Arduino.h>

#ifndef DISPLAY_H
#define DISPLAY_H

void setupDisplay();
void displayStatus(String);
void displayTemperature(float);
void displayHumidity(float);

#endif
