#ifndef PINS_H
#define PINS_H

// Output Pins
#define PIN_COOLER 12
#define PIN_CIRCULATION 13
#define PIN_HUMIDIFIER 2

// Input Pins
#define PIN_DHT 14
#define PIN_HX711_CLK 0
#define PIN_HX711_DATA 0

#endif
