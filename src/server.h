#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

#ifndef SERVER_H
#define SERVER_H

void setupServer();
void loopServer();

#endif
