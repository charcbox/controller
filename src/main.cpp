#include <Arduino.h>
#include <EEPROM.h>
#include "display.h"
#include "wifi.h"
#include "server.h"
#include "sensors.h"
#include "outputs.h"
#include "control.h"
#include "settings.h"

long lastRead = 0;

void setup() {
  delay(5000);
  EEPROM.begin(512);
  setupDisplay();
  loadSettings();
  setupWifi();
  setupSensors();
  setupOutputs();
  setupServer();
}

void loop() {
  long time = millis();
  if (time - lastRead > 2000) {
    lastRead = time;
    loopTemperatureHumiditySensor();
    loopControl();
  }
  loopServer();
}
