#include "server.h"
#include "sensors.h"
#include "settings.h"

ESP8266WebServer server(8080);

int currentWeight = 100;

void handleGetStatus() {
  StaticJsonBuffer<512> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();

  JsonObject& live = root.createNestedObject("live");
  live["temperature"] = currentTemperature;
  live["humidity"] = currentHumidity;
  live["weight"] = currentWeight;

  JsonObject& settings = root.createNestedObject("settings");
  settings["temperature"] = targetTemperature;
  settings["humidity"] = targetHumidity;
  settings["weight"] = targetWeight;

  char buffer[512];
  root.printTo(buffer, sizeof(buffer));
  server.send(200, "application/json", buffer);
}

void handleGetSettings() {
  StaticJsonBuffer<512> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();

  root["temperature"] = targetTemperature;
  root["humidity"] = targetHumidity;
  root["weight"] = targetWeight;

  char buffer[512];
  root.printTo(buffer, sizeof(buffer));
  server.send(200, "application/json", buffer);
}

void handlePutSettings() {
  StaticJsonBuffer<512> jsonBuffer;

  JsonObject& root = jsonBuffer.parseObject(server.arg("plain"));

  if (!root.success()) {
    server.send(400, "application/json", "{\"error\":\"Unparsable Request\"}");
    return;
  }

  if(root.containsKey("temperature"))
    setTargetTemperature(root["temperature"].as<uint8_t>());

  if(root.containsKey("humidity"))
    setTargetHumidity(root["humidity"].as<uint8_t>());

  if(root.containsKey("weight"))
    setTargetWeight(root["weight"].as<uint16_t>());

  saveSettings();

  handleGetSettings();
}

void handleNotFound() {
  server.send(404, "application/json", "{\"error\":\"Not Found\"}");
}

void setupServer() {
  MDNS.addService("http", "tcp", 80);
  server.on("/status", HTTP_GET, handleGetStatus);
  server.on("/settings", HTTP_GET, handleGetSettings);
  server.on("/settings", HTTP_PUT, handlePutSettings);
  server.on("/settings", HTTP_PATCH, handlePutSettings);
  server.onNotFound(handleNotFound);
  server.begin();
}

void loopServer() {
  server.handleClient();
}
