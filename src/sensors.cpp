#include "sensors.h"
#include "pins.h"
// #include <HX711.h>
#include "DHT.h"
#include <ESP8266HTTPClient.h>

DHT dht(PIN_DHT, DHT22);

float currentTemperature = -1;
float currentHumidity = -1;

void setupSensors() {
  dht.begin();
}

void sendSettingsToServer(float temperature, float humidity) {
  HTTPClient http;
  String buffer;

  buffer += "{";
  buffer += "\"temperature\": " + String(temperature);
  buffer += ", ";
  buffer += "\"humidity\": " + String(humidity);
  buffer += "}";

  http.begin("http://localhost:3001/settings");
  http.addHeader("Content-Type", "application/json");
  http.POST(buffer);
  http.writeToStream(&Serial);
  http.end();
}

void loopTemperatureHumiditySensor() {
  currentTemperature = dht.readTemperature();
  currentHumidity = dht.readHumidity();

  Serial.print("Read: Temperature ");
  Serial.print(currentTemperature);
  Serial.print(" Humidity ");
  Serial.println(currentHumidity);
  sendSettingsToServer(currentTemperature, currentHumidity);
}
