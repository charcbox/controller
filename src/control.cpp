#include "control.h"
#include "sensors.h"
#include "outputs.h"
#include "settings.h"

bool coolingState = false;
bool circulationState = false;
bool humidifierState = false;

long circulationTimeout = -1;

#define CIRCULATION_TIMEOUT 120

void loopControl() {
  if(currentTemperature > targetTemperature) {
    if(!coolingState) {
      setCooling(coolingState = true);
      setCirculation(circulationState = true);
    }
  }
  else {
    if(coolingState) {
      setCooling(coolingState = false);
      circulationTimeout = millis() + CIRCULATION_TIMEOUT * 1000; // 2 mins after cooling stops
    }

    if(circulationState) {
      if(circulationTimeout < 0) {
        setCirculation(circulationState = false);
      } else if(circulationTimeout < millis()) {
        setCirculation(circulationState = false);
        circulationTimeout = -1;
      }
    }
  }

  if(currentHumidity < targetHumidity) {
    if(!humidifierState)
      setHumidifier(humidifierState = true);
  }
  else {
    if(humidifierState)
      setHumidifier(humidifierState = false);
  }
}
