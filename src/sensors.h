#include "pins.h"

#ifndef SENSORS_H
#define SENSORS_H

extern float currentTemperature;
extern float currentHumidity;
// extern float currentWeight;

void setupSensors();
void loopTemperatureHumiditySensor();

#endif
