#include "settings.h"
#include "display.h"
#include <EEPROM.h>

uint8_t targetTemperature;
uint8_t targetHumidity;
uint16_t targetWeight;

#define CONFIG_START 0

struct Settings {
  uint8_t temperature;
  uint8_t humidity;
  uint16_t weight;
};

void debug() {
  Serial.print("targetTemperature: ");
  Serial.println(targetTemperature);
  Serial.print("targetHumidity: ");
  Serial.println(targetHumidity);
  Serial.print("targetWeight: ");
  Serial.println(targetWeight);
}

void loadSettings() {
  displayStatus("Loading Settings");
  Settings saved;
  EEPROM.get(CONFIG_START, saved);
  targetTemperature = saved.temperature;
  targetHumidity = saved.humidity;
  targetWeight = saved.weight;
  debug();
}

void saveSettings() {
  displayStatus("Saving Settings");
  Settings current = { targetTemperature, targetHumidity, targetWeight };
  EEPROM.put(CONFIG_START, current);
  EEPROM.commit();
  debug();
  loadSettings();
}

void setTargetTemperature(uint8_t temperature) {
  targetTemperature = temperature;
}

void setTargetHumidity(uint8_t humidity) {
  targetHumidity = humidity;
}

void setTargetWeight(uint16_t weight) {
  targetWeight = weight;
}
