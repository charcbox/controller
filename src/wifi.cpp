#include "wifi.h"
#include "display.h"
#include <ESP8266WiFi.h>          //ESP8266 Core WiFi Library (you most likely already have this in your sketch)
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic

WiFiManager wifiManager;

void setupMDNS(const char* hostname) {
  if (!MDNS.begin(hostname)) {
    displayStatus("MDNS Error");
    while(1) {
      // Die...
      delay(1000);
    }
  }
}

void setupWifi() {
  wifiManager.resetSettings();
  wifiManager.autoConnect("CharcBox-Setup");
  setupMDNS("charcbox");
}
