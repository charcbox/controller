#ifndef OUTPUTS_H
#define OUTPUTS_H

void setupOutputs();
void setCooling(bool);
void setCirculation(bool);
void setHumidifier(bool);

#endif
