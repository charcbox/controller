#include <Arduino.h>
#include <ArduinoJson.h>

#ifndef SETTINGS_H
#define SETTINGS_H

extern uint8_t targetTemperature;
extern uint8_t targetHumidity;
extern uint16_t targetWeight;

void loadSettings();
void saveSettings();

void setTargetTemperature(uint8_t temperature);
void setTargetHumidity(uint8_t humidity);
void setTargetWeight(uint16_t weight);

#endif
