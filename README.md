### Software Needed
- Install platform.io Atom plugin
- ```brew install picocom```


### ESP Firmware Upload
1. Sequence commands
  - Press Power switch
  - Press Flash switch
  - Release Power switch
  - Release Flash switch
2. Inside Platform.Io, click "Upload"

### Debugging ESP from Mac
```
picocom /dev/tty.usbserial-A60409U3 -b 115200
```


### Miscs

- External libs folder: ```cd .piolibsdeps```
